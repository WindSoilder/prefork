use std::{net::TcpListener, thread, time::Duration};

use log::info;
use prefork::Prefork;

fn child(child_num: u32, _listener: TcpListener) {
    info!("Starting synchronous child {child_num}");
    thread::sleep(Duration::from_secs(10));
    info!("Stopping synchronous child {child_num}");
}

fn main() {
    // Send logs to tracing crate.
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::DEBUG)
        .without_time()
        .init();

    let listener = TcpListener::bind("0.0.0.0:3000").expect("bind to port");
    let is_parent = Prefork::from_resource(listener)
        .with_num_processes(10)
        .with_init(child)
        .fork()
        .expect("fork");
    if is_parent {
        info!("Parent exit");
    }
}
